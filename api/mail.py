# Module used to interface with the sendgrip api 
import sendgrid
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail as SendGrid
from os import getenv
import config
class Mail:
	def __init__(self):
            self.key = getenv('MAIL_KEY')
            self.dev_mail = getenv('DEV_MAIL')

	def _user_response(self, api_response):
            code = api_response.status_code
            if code == 200 or code == 202:
                    return 'Sent!'
            else:
                    return f'Server issues, my email on the right always works however!'

	def __html_template(self, message):
	    return f'<h1>Possible Inquiry</h1><div>{message}</div>'

	def send(self, form):
            '''
            main handler to build the api query
            From here we can actually tell the api to send off an email to us
            '''
            msg = SendGrid(
                from_email = form['email'],
                to_emails = self.dev_mail,
                subject = form['subject'],
                # this bit is just here to avoid spam filters
                html_content = self.__html_template(form['details'])
            )
            try:
                sg_client = SendGridAPIClient(self.key)
                response = sg_client.send(msg)
                print(response.status_code)
                print(response.body)
                print(response.headers)
                # finally respond the user 
                return self._user_response(response)
            except Exception as e:
                return f'Mail server appears to not be responding'
