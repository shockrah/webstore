# Module for some basic checks

import re
# setup some basic module things
_str_email_regex = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
email_regex = re.compile(_str_email_regex)

def verify_form_data(form : dict) -> bool:
	'''
	ID's generally unwanted info from a form
	Return: False if something in form doesn't look right
	Reutrn: True if form looks fine
	'''
	for k in form:
		# Check for any empty params
		if form[k] == '':
			return False
			
	return True

def verify_email(mail : str) -> bool:
	global email_regex
	return re.match(email_regex, mail)


