# Flask config
import os
templates='./templates'
# Should be set to false on prod, true on dev env
DEBUG = False
if os.getenv('DEBUG') == 'true':
    DEBUG = True

# Sendgrid api key
MAIL_KEY = os.getenv('MAIL_KEY')
# Email address target for the api
DEV_MAIL = os.getenv('DEV_MAIL')
