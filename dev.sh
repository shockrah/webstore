#!/bin/bash

function deploy() {
	echo '=== Pushing to gitlab.com ==='
	git push origin master
	echo '=== Done with gitlab.com ==='
	echo '=== Pushing to heroku.com ==='
	git push heroku master
	echo '=== Done with heroku.com ==='
}

function server() {
	source ./bin/activate
	source ./kek
	python router.py
}

function heroku_local() {
	source ./bin/activate
	heroku local
}

function setup() {
	if [ ! -d ./bin/ ]
	then
		virtualenv .
		source bin/activate
		pip install -r ./requirements.txt
	fi
}

function _help() {
	echo 'Options:'
	echo '	-s :	start development server'
	echo '	-i :	setup virtualenv'
	echo '	-d : 	Deploy to gitlab and heroku(master branches)'
	echo '	-l : 	Local heroku instance'
}

if [ -z $1 ];then
	_help
	exit 1
fi
# Going through through options
while getopts ':dsilh' OPTION; do
	case "$OPTION" in
		'd')
			deploy
			echo 'Deploying to heroku'
			;;
		's')
			server
			;;
		'i')
			setup
			;;
		'h')
			_help
			;;
		'l')
			heroku_local
			;;
		*)
			;;
	esac
	
done
