# Shockrah's Shop

> I make stuff on the internet

If you're looking for some webservices or maybe even a tutor to learn some programming then don't delay and buy today!

# Backend 

Running on the latest version of Flask, which is primarily used for URL routing.
Some form validation is done but not much because there is currently only one form at the moment.

# Building/Running

_NOTE_: There is a `config.py` file where some environment variables are used to access certain remote API's, like the SendGrid API I use for sending form data through an email.

## Pullin in dependancies

To keep things clean it is recommended to install all dependedancies in a [Virtual Environment].

1. Install `virtualenv` to make things cleaner for development. _If you're not going to use `virtualenv` then skip to step 3.

2. Next open a terminal and change to the project's root directory. Issue a `virtualenv .` to create a virtual environment in the project's root directory.

3. By doing `source bin/activate`(Linux) the virtual environment activates, allowing to use `pip install -r requirements.txt` to install the project dependencies.

4. To setup the environment variables use the example script provided below. Save the content of the below script into a new file.
Name it whatever seems convenient and back in the terminal run `source name-of-file` to export those environment variables to your current shell session.

5. In the same terminal window `python router.py` should start the Flask application and you should now be able to use locally hosted web server normally.


Setting up environment vars should be as simple as `source`ing a shell script to export the necessary variables.

Here is an example script which can be used for development

```sh
#!/bin/sh
export DEBUG='true'
export MAIL_KEY='aaabbbccc'
export DEV_MAIL='your-email-here@mail.org'
```

It should be noted that the SendGrid feature _will not work_ without a valid API Key.
One can be acquired on the [official website](https://sendgrid.com/).
