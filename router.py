from flask import Flask
from flask import render_template, url_for
from flask import request 

from api import mail
from api.mail import Mail
from api.verify import verify_form_data, verify_email

import config


app = Flask('main router', template_folder=config.templates)

@app.route('/')
def home():
    return render_template('index.jinja')

@app.route('/store')
def store():
    return render_template('store.jinja')

@app.route('/store/<page>')
def store_pages(page):
    return render_template(f'{page}.jinja')

# This pages is just a giant list of templates that I provide
# all of which are one page templates
@app.route('/templates')
def templates():
    return render_template('templates.jinja')

@app.route('/terms')
def terms():
    return render_template('contract_src/contract.jinja')

@app.route('/examples/<page>')
def examples(page):
    # route just for the example pages
    return render_template(f'examples/{page}.jinja')

# Only bothering with post's to the mail endpoint
@app.route('/mail', methods=['POST'])
def handle_mail():
    # Falthrough for handling the request
    # TODO: make sure these check pass properly with expected behavior
    if verify_form_data(form=request.form):
        if verify_email(request.form['email']):
        # then we can tell our api to send our emil through sendgrid
            return Mail().send(request.form)
        else:
            return 'Check email spelling'
    else:
        return 'Check to make sure all fields are filled correctly'

    return 'Backend seems to be offline try emailing me directly instead'
            
		
if __name__ == '__main__':
	app.run(debug=config.DEBUG)
