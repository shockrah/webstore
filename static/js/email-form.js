function success_cb() {
    $('#response').text('Sucess! I look forward to working with you :^)')
}
function error_cb() {
    $('#response').text('Email server is likely down at the moment try emailing me directly instead');
}
function sendmail() {
    $('#loading').css('font-size', '24px');
    $.ajax({
        type: 'POST',
        url: '/mail',
        data: {
            'subject': $('#subject').val(),
            'email': $('#email').val(),
            'details': $('#details').val()
        },
        success: success_cb,
        error: error_cb
    })
    .done(function() {
        $('#submit').css('display', 'none');
    });
}
$(document).ready(function() {
    $('#submit').click(sendmail);
});