// Module to deal with POSTing form-data and dealing with the response
function divEl(divID) {
	return document.getElementById(divID);
}

function post(url, formData, responseID=false) {
	// Set the display for the loading thingy
	divEl('loading').style = 'font-size:24px'
	// Generate the post data to send to the 
	var xhr = new XMLHttpRequest();
	var fd = new FormData();
	for(item in formData) {
		fd.append(item, formData[item]);
	}
	if(responseID) {
		xhr.addEventListener('load', function(event) {
			divEl(responseID).innerHTML = event.target.response;
			divEl('loading').style = 'display:none;font-size:24px';
		});
	}
	xhr.open('POST',url);
	xhr.send(fd);
}
